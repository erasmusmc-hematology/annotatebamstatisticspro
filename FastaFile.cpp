//----------------------------------------------------------------
// Name        : FastaFile.cpp
// Author      : Remco Hoogenboezem
// Version     :
// Copyright   :
// Description : 
//----------------------------------------------------------------
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "Alphabet.h"
#include "FastaFile.h"
//----------------------------------------------------------------
CFastaFile::CFastaFile(void)
{
    pSequence=NULL;
}
//----------------------------------------------------------------
CFastaFile::~CFastaFile(void)
{
    Clear();
}
//----------------------------------------------------------------
bool CFastaFile::Open(const string & sFileName)
{
    bool                bGood;
    
    char                cLine[1048576];
    
    long                lFileSize;

    FILE *              pFastaFile;
    int8_t *            pSequence;
    int8_t *            pTarget;
    char *              pLine;
    
    TARGET              target;

    string              sTargetName;
    
    map<string,TARGET>  targets;
    
    //----------------------------------------------------------------
    //Remove previous sequences
    //----------------------------------------------------------------
    
    Clear();
    
    //----------------------------------------------------------------
    //Open the fasta file
    //----------------------------------------------------------------
    
    if((pFastaFile=fopen(sFileName.c_str(),"r"))==NULL)
    {
        cerr << "Error: Could not open fasta file: " << sFileName << endl;
        return false;
    }
    
    //----------------------------------------------------------------
    //Compute file size
    //----------------------------------------------------------------
    
    fseek(pFastaFile,0L,SEEK_END);
    lFileSize=ftell(pFastaFile);
    fseek(pFastaFile,0L,SEEK_SET);
    
    //----------------------------------------------------------------
    //Allocate memory 
    //----------------------------------------------------------------
    
    pSequence=pTarget=new int8_t[lFileSize];    
    
    //----------------------------------------------------------------    
    //Read fasta file 
    //----------------------------------------------------------------

    for(bGood=fgets(cLine,1048576,pFastaFile)!=NULL;bGood;)
    {
        if(cLine[0]=='>')
        {
            cLine[strcspn(cLine,"\n ")]='\0';
            
            sTargetName=string(cLine+1);
            target.pTarget=pTarget;
            
            for(bGood=fgets(cLine,1048576,pFastaFile)!=NULL;bGood;bGood=fgets(cLine,1048576,pFastaFile)!=NULL)
            {
                if(cLine[0]=='>')
                {
                    break;
                }
                
                for(pLine=cLine;pLine[0]!='\0' && pLine[0]!='\n';)
                {
                    *pTarget++=int8_t(iAscii2BamSeq[*pLine++]);
                }
            }
            
            target.iTargetLength=pTarget-target.pTarget;
            targets[sTargetName]=target;
        }
        
        else
        {
            bGood=fgets(cLine,1048576,pFastaFile)!=NULL;
        }
    }
    
    //----------------------------------------------------------------
    //End of file?
    //----------------------------------------------------------------
    
    if(feof(pFastaFile)==0)
    {
        cerr << "Error: Could not read fasta file: " << sFileName << endl;

        fclose(pFastaFile);
        delete [] pSequence;        
        return false;
    }
    
    fclose(pFastaFile);
    
    //----------------------------------------------------------------
    //Save results
    //----------------------------------------------------------------
    
    this->pSequence=pSequence;
    this->targets=targets;
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------
    
    return true;
}
//----------------------------------------------------------------
void CFastaFile::Clear(void)
{
    if(pSequence)
    {
        delete [] pSequence;
        pSequence=NULL;
    }
    
    targets.clear();
}
//----------------------------------------------------------------