//----------------------------------------------------------------
#ifndef VCFFileH
#define	VCFFileH
//----------------------------------------------------------------
#include <map>
#include <string>
//----------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------
class CVCFFile
{
    private:
        
    public:
        
        map<string,multimap<int,int> > mEntries;
        
        bool Open(const string & sFileName);
};
//----------------------------------------------------------------
#endif