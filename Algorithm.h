//----------------------------------------------------------------
#ifndef AlgorithmH
#define AlgorithmH
//----------------------------------------------------------------
#include <string>
#include <vector>
#include <deque>
#include "AnnovarFile.h"
#include "FastaFile.h"
#include "VCFFile.h"
#include "BamFile.h"
#include "Mutex.h"
//----------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------
typedef struct
{
    int     iBamFile;
    int     iPos;
    string  sTarget;
    
}JOB;
//----------------------------------------------------------------
typedef struct
{
    int     iDepth;
    float   fBias;
    int     iAltDepth;
    float   fAltBias;

    int     iHQDepth;
    float   fHQBias;
    int     iHQAltDepth;
    float   fHQAltBias;

    float   fDuplicates;
    float   fRefSkip;
    float   fSecondary;
    float   fSecondaryAlt;
    float   fOrphan;
    float   fOrphanAlt;
    float   fAnomalous;
    float   fAnomalousAlt;
    float   fAlternatives;
    float   fAlternativesAlt;
    float   fSuboptimal;
    float   fSuboptimalAlt;
    float   fClipped;
    float   fClippedAlt;
    float   fMatchLengthTooLow;
    float   fMatchLengthTooLowAlt;
    float   fMismatchesTooHigh;
    float   fMismatchesTooHighAlt;

    float   fGenotypingError;
    
}STATISTICS;
//----------------------------------------------------------------
typedef struct
{
    bool                bCountDuplicates;
    bool                bSecondaryIsHQ;
    bool                bOrphanIsHQ;
    bool                bAnomalousIsHQ;
    bool                bHasAlternativesIsHQ;
    bool                bSuboptimalIsHQ;
    bool                bClippedIsHQ;
    bool                bMatchLengthTooLowIsHQ;
    bool                bMismatchesTooHighIsHQ;

    int                 iAlt;
    int                 iLen;
    int                 iMinBaseScore;
    int                 iMinMismatchBaseScore;
    int                 iMinAlignmentScore;    
    int                 nMaxMismatches;
    int                 iMinDiffSuboptimal;

    double              dMinMatchLength;

    PLP_DATA *          pPileupData;
    int8_t *            pTarget;
    multimap<int,int> * pVCFFileEntries;
    STATISTICS *        pStatistics;

}COMPUTE_STATISTICS;
//----------------------------------------------------------------
class CAlgorithm
{
    private:
        
        int             nJobsDone;

        CAnnovarFile    annovarFile;
        CFastaFile      fastaFile;
        CVCFFile        vcfFile;
        CMutex          mutex;
        
        deque<JOB>      jobs;
        
        static string & SampleNameFromPath(const string & sPath);

        static void     ComputeStatistics(COMPUTE_STATISTICS & pComputeStatistics);
        static void *   Pileup(CAlgorithm * pAlgorithm);
        
        void CreateOutputMatrix(void);
        void CreateJobs(void);
        void RunJobs(void);
        void WriteOutputMatrix(void);
        
    public:

        bool            bCountDuplicates;
        bool            bPileupRegions;
        bool            bVerbose;
        bool            bSecondaryIsHQ;
        bool            bOrphanIsHQ;
        bool            bAnomalousIsHQ;
        bool            bHasAlternativesIsHQ;
        bool            bSuboptimalIsHQ;
        bool            bClippedIsHQ;
        bool            bMatchLengthTooLowIsHQ;
        bool            bMismatchesTooHighIsHQ;
        
        int             nThreads;
        int             iMinBaseScore;
        int             iMinMismatchBaseScore;        
        int             iMinAlignmentScore;
        int             nMaxMismatches;
        int             iMinDiffSuboptimal;
        
        double          dMinMatchLength;

        vector<string>  vBamFileNames;
        string          sAnnovarFileName;
        string          sFastaFileName;
        string          sVCFFileName;
        
        CAlgorithm(void);
        bool Run(void);
};
//----------------------------------------------------------------
#endif
