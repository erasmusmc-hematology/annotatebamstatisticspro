//----------------------------------------------------------------
// Name        : Algorithm.cpp
// Author      : Remco Hoogenboezem
// Version     :
// Copyright   :
// Description :
//----------------------------------------------------------------
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <string.h>
#include <unistd.h>
#include "Alphabet.h"
#include "ProgressBar.h"
#include "Algorithm.h"
//----------------------------------------------------------------
string & CAlgorithm::SampleNameFromPath(const string & sPath)
{
    size_t          lastSlashPos;
    size_t          lastPeriodPos;
    static string   sSampleName;
    
    sSampleName=sPath;
    
    lastSlashPos=sSampleName.find_last_of("\\/");

    if(lastSlashPos!=string::npos)
    {
        sSampleName.erase(0,lastSlashPos+1);
    }

    lastPeriodPos=sSampleName.rfind('.');

    if(lastPeriodPos!=string::npos)
    {
        sSampleName.erase(lastPeriodPos);
    }
    
    return sSampleName;
}
//----------------------------------------------------------------
void CAlgorithm::ComputeStatistics(COMPUTE_STATISTICS & computeStatistics)
{
    bool                    bCountDuplicates;
    bool                    bSecondaryIsHQ;
    bool                    bOrphanIsHQ;
    bool                    bAnomalousIsHQ;
    bool                    bHasAlternativesIsHQ;
    bool                    bSuboptimalIsHQ;
    bool                    bClippedIsHQ;
    bool                    bMatchLengthTooLowIsHQ;
    bool                    bMismatchesTooHighIsHQ;
    bool                    bIsSNV;
    bool                    bIsAlt;
    bool                    bIsHQ;
    bool                    bIsMismatch;
    
    int                     iAlt;
    int                     iLen;
    int                     iMinBaseScore;
    int                     iMinMismatchBaseScore;
    int                     iMinAlignmentScore;
    int                     nMaxMismatches;
    int                     iMinDiffSuboptimal;
    int                     nAlignments;
    int                     iAlignment;
    int                     nDuplicates;
    int                     nRefSkip;
    int                     iReadDepth[2];
    int                     iAltReadDepth[2];
    int                     nSecondary;
    int                     nSecondaryAlt;
    int                     nOrphan;
    int                     nOrphanAlt;
    int                     nAnomalous;
    int                     nAnomalousAlt;
    int                     nAlternatives;
    int                     nAlternativesAlt;
    int                     nSuboptimal;
    int                     nSuboptimalAlt;
    int                     nClipped;
    int                     nClippedAlt;
    int                     nMatchLengthTooLow;
    int                     nMatchLengthTooLowAlt;
    int                     nMismatchesTooHigh;
    int                     nMismatchesTooHighAlt;
    int                     iHQReadDepth[2];
    int                     iHQAltReadDepth[2];
    int                     iBaseScore;
    int                     iAlignmentScore;
    int                     iStrand;
    int                     nOperations;
    int                     iOperation;
    int                     iMatchLength;
    int                     nMismatches;
    int                     iBamSeqPos;
    int                     iReferencePos;
    int                     iOpLen;
    int                     iBase;
    int                     iTotalReadDepth;
    int                     iDepth;
    int                     iAltDepth;
    int                     iHQDepth;
    int                     iHQAltDepth;
    int                     iQueryPos;
    int                     nGenotypeErrors;
    
    uint32_t                dwFlag;
    uint32_t                dwFirstOp;
    uint32_t                dwLastOp;
    
    double                  dMinMatchLength;
    double                  dInvTotalReadDepth;
    double                  dInvTotalAltReadDepth;
    
    const bam_pileup1_t *   pAlignments;
    int8_t *                pTarget;
    multimap<int,int> *     pVCFFileEntries;
    STATISTICS *            pStatistics;
    
    const bam_pileup1_t *   pMate;
    const bam1_t *          pAlignments_b;
    uint8_t *               pAlignmentScore;
    uint8_t *               pSuboptimalScore;
    uint32_t *              pCIGAR;
    uint8_t *               pBamSeq;
    uint8_t *               pBamQual;
    
    string                  sQueryName;

    unordered_map<string,const bam_pileup1_t *>             mAlignments;
    unordered_map<string,const bam_pileup1_t *>             mHQAlignments;    
    unordered_map<string,const bam_pileup1_t *>::iterator   itAlignments;
    unordered_map<string,const bam_pileup1_t *>::iterator   itAlignmentsEnd;
    
    pair<multimap<int,int>::iterator,multimap<int,int>::iterator>   equalRange;
    multimap<int,int>::iterator                                     itEqualRange;
    
    
    //----------------------------------------------------------------
    //Initialize
    //----------------------------------------------------------------
    
    bCountDuplicates=computeStatistics.bCountDuplicates;
    bSecondaryIsHQ=computeStatistics.bSecondaryIsHQ;
    bOrphanIsHQ=computeStatistics.bOrphanIsHQ;
    bAnomalousIsHQ=computeStatistics.bAnomalousIsHQ;
    bHasAlternativesIsHQ=computeStatistics.bHasAlternativesIsHQ;
    bSuboptimalIsHQ=computeStatistics.bSuboptimalIsHQ;
    bClippedIsHQ=computeStatistics.bClippedIsHQ;
    bMatchLengthTooLowIsHQ=computeStatistics.bMatchLengthTooLowIsHQ;
    bMismatchesTooHighIsHQ=computeStatistics.bMismatchesTooHighIsHQ;
    
    iAlt=computeStatistics.iAlt;
    iLen=computeStatistics.iLen;
    iMinBaseScore=computeStatistics.iMinBaseScore;
    iMinMismatchBaseScore=computeStatistics.iMinMismatchBaseScore;
    iMinAlignmentScore=computeStatistics.iMinAlignmentScore;
    nMaxMismatches=computeStatistics.nMaxMismatches;
    iMinDiffSuboptimal=computeStatistics.iMinDiffSuboptimal;
    
    dMinMatchLength=computeStatistics.dMinMatchLength;
    
    nAlignments=computeStatistics.pPileupData->nAlignments;
    pAlignments=computeStatistics.pPileupData->pAlignments;
    pTarget=computeStatistics.pTarget;
    pVCFFileEntries=computeStatistics.pVCFFileEntries;
    pStatistics=computeStatistics.pStatistics;

    bIsSNV=(iAlt!=INS)&&(iAlt!=DEL);
    
    nDuplicates=0;
    nRefSkip=0;
    iReadDepth[0]=iReadDepth[1]=0;
    iAltReadDepth[0]=iAltReadDepth[1]=0;
    nSecondary=0;
    nSecondaryAlt=0;
    nOrphan=0;
    nOrphanAlt=0;
    nAnomalous=0;
    nAnomalousAlt=0;
    nAlternatives=0;
    nAlternativesAlt=0;
    nSuboptimal=0;
    nSuboptimalAlt=0;
    nClipped=0;
    nClippedAlt=0;
    nMatchLengthTooLow=0;
    nMatchLengthTooLowAlt=0;
    nMismatchesTooHigh=0;
    nMismatchesTooHighAlt=0;
    iHQReadDepth[0]=iHQReadDepth[1]=0;
    iHQAltReadDepth[0]=iHQAltReadDepth[1]=0;
    nGenotypeErrors=0;
    
    //----------------------------------------------------------------
    //Iterate over reads
    //----------------------------------------------------------------
    
    for(iAlignment=0;iAlignment<nAlignments;iAlignment++,pAlignments++)
    {
        //----------------------------------------------------------------
        //Count duplicate fragments (Skip if we do not want to count duplicates)
        //----------------------------------------------------------------

        pAlignments_b=pAlignments->b;
        dwFlag=pAlignments_b->core.flag;
        
        if(dwFlag&BAM_FDUP)
        {
            nDuplicates++;
            
            if(bCountDuplicates==false)
            {
                continue;
            }
        }
                
        //----------------------------------------------------------------
        //Count refskip reads
        //----------------------------------------------------------------
        
        if((pAlignments->is_del)||(pAlignments->is_refskip))
        {
            nRefSkip++;
            continue;
        }

        //----------------------------------------------------------------
        //Alignment is alt?
        //----------------------------------------------------------------
        
        pBamSeq=bam1_seq(pAlignments_b);
        iQueryPos=pAlignments->qpos;
        
        if(bIsSNV)  
        {
            bIsAlt=int(bam1_seqi(pBamSeq,iQueryPos))==iAlt;
        }
        
        else
        {
            bIsAlt=pAlignments->indel==iLen;
        }

        
        //----------------------------------------------------------------
        //Compute bias 
        //----------------------------------------------------------------
        
        iStrand=int(bam1_strand(pAlignments_b));
        iReadDepth[iStrand]++;
        iAltReadDepth[iStrand]+=bIsAlt;
        
        //----------------------------------------------------------------
        //Base & alignment score HQ?
        //----------------------------------------------------------------
        
        pBamQual=bam1_qual(pAlignments_b);
        iBaseScore=int(pBamQual[iQueryPos]);
        iAlignmentScore=int(pAlignments_b->core.qual);
        
        bIsHQ=(iBaseScore>=iMinBaseScore) && (iAlignmentScore>=iMinAlignmentScore);
        
        //----------------------------------------------------------------
        //Alignment is secondary?
        //----------------------------------------------------------------
        
        if(dwFlag&BAM_FSECONDARY)
        {
            bIsHQ=bIsHQ && bSecondaryIsHQ;
            nSecondary++;
            nSecondaryAlt+=bIsAlt;
        }

        //----------------------------------------------------------------
        //Alignment is orphan?
        //----------------------------------------------------------------
        
        if((dwFlag&BAM_FPAIRED)!=0 && (dwFlag&BAM_FMUNMAP)!=0)
        {
            bIsHQ=bIsHQ && bOrphanIsHQ;
            nOrphan++;
            nOrphanAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Alignment is anomalous?
        //----------------------------------------------------------------
        
        if((dwFlag&BAM_FPAIRED)!=0 && (dwFlag&BAM_FMUNMAP)==0 && (dwFlag&BAM_FPROPER_PAIR)==0)
        {
            bIsHQ=bIsHQ && bAnomalousIsHQ;
            nAnomalous++;
            nAnomalousAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Alignment has alternatives? using bwa flags
        //----------------------------------------------------------------
        
        if(bam_aux_get(pAlignments_b,"XA")!=NULL)
        {
            bIsHQ=bIsHQ && bHasAlternativesIsHQ;
            nAlternatives++;
            nAlternativesAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Alignment is suboptimal ? using bwa flags
        //----------------------------------------------------------------

        pAlignmentScore=bam_aux_get(pAlignments_b,"AS");
        pSuboptimalScore=bam_aux_get(pAlignments_b,"XS");
        
        if((pAlignmentScore!=NULL)&&(pSuboptimalScore!=NULL))
        {
            if((bam_aux2i(pAlignmentScore)-bam_aux2i(pSuboptimalScore))<=iMinDiffSuboptimal)
            {
                bIsHQ=bIsHQ && bSuboptimalIsHQ;
                nSuboptimal++;
                nSuboptimalAlt+=bIsAlt;
            }
        }
        
        //----------------------------------------------------------------
        //Alignment is clipped?
        //----------------------------------------------------------------
        
        nOperations=pAlignments_b->core.n_cigar;
        pCIGAR=bam1_cigar(pAlignments_b);
        dwFirstOp=bam_cigar_op(pCIGAR[0]);
        dwLastOp=bam_cigar_op(pCIGAR[nOperations-1]);
        
        if((dwFirstOp==BAM_CSOFT_CLIP) || (dwFirstOp==BAM_CHARD_CLIP) || (dwLastOp==BAM_CHARD_CLIP) || (dwLastOp==BAM_CSOFT_CLIP))
        {
            bIsHQ=bIsHQ && bClippedIsHQ;
            nClipped++;
            nClippedAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Compute match length and number of mismatches in alignment
        //----------------------------------------------------------------
        
        iMatchLength=0;
        nMismatches=0;

        iBamSeqPos=0;
        iReferencePos=pAlignments_b->core.pos;
        
        for(iOperation=0;iOperation<nOperations;iOperation++,pCIGAR++)
        {
            iOpLen=int(bam_cigar_oplen(*pCIGAR));
            
            switch(bam_cigar_op(*pCIGAR))
            {
                case BAM_CMATCH:        
                case BAM_CEQUAL:
                case BAM_CDIFF:

                    iMatchLength+=iOpLen;

                    for(;iOpLen>0;iOpLen--,iBamSeqPos++,iReferencePos++)
                    {
                        iBase=int(bam1_seqi(pBamSeq,iBamSeqPos));
                        
                        if(iBamSeqPos==iQueryPos)   //Current query position count the number of genotype errors
                        {
                            nGenotypeErrors+=((iBase!=int(pTarget[iReferencePos])) && (iBase!=iAlt)) || ((pAlignments->indel!=0) && (pAlignments->indel!=iLen));
                            continue;
                        }
                        
                        if((iBase==int(pTarget[iReferencePos])) || (int(pBamQual[iBamSeqPos])<iMinMismatchBaseScore))
                        {
                            continue;
                        }

                        bIsMismatch=true;

                        if(pVCFFileEntries)
                        {
                            equalRange=pVCFFileEntries->equal_range(iReferencePos);

                            for(itEqualRange=equalRange.first;itEqualRange!=equalRange.second;itEqualRange++)
                            {
                                if(iBase==itEqualRange->second)
                                {
                                    bIsMismatch=false;
                                    break;
                                }
                            }
                        }

                        nMismatches+=bIsMismatch;
                    }
                
                    break;
                
                case BAM_CINS:          
                    
                    iBamSeqPos+=iOpLen;
                    break;
                
                case BAM_CDEL:          

                    iReferencePos+=iOpLen;
                    break;

                case BAM_CSOFT_CLIP:
                    
                    iBamSeqPos+=iOpLen;
                    break;
                    
                case BAM_CREF_SKIP:                
                
                    iReferencePos+=iOpLen;
                    break;
            }
        }
        
        //----------------------------------------------------------------
        //Alignment match length too low?
        //----------------------------------------------------------------
        
        if(double(iMatchLength)/double(max(pAlignments_b->core.l_qseq,1))<dMinMatchLength)
        {
            bIsHQ=bIsHQ && bMatchLengthTooLowIsHQ;
            nMatchLengthTooLow++;
            nMatchLengthTooLowAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Alignment number of mismatches too high
        //----------------------------------------------------------------
        
        if(nMismatches>nMaxMismatches)
        {
            bIsHQ=bIsHQ && bMismatchesTooHighIsHQ;
            nMismatchesTooHigh++;
            nMismatchesTooHighAlt+=bIsAlt;
        }
        
        //----------------------------------------------------------------
        //Accumulate fragments
        //----------------------------------------------------------------
        
        sQueryName=string(bam1_qname(pAlignments_b));
        itAlignments=mAlignments.find(sQueryName);
        
        if(itAlignments==mAlignments.end())                             //This name does not already exist?
        {
            mAlignments[sQueryName]=pAlignments;
        }
        
        else                                                            //This name does already exist
        {
            pMate=itAlignments->second;
            
            if(bIsSNV)
            {
                if(bam1_qual(pMate->b)[pMate->qpos]<iBaseScore)         //For SNPs keep base with best score
                {
                    mAlignments[sQueryName]=pAlignments;
                }
            }
            
            else
            {
                if((pMate->indel==iLen)==bIsAlt)                        //If both mates show the same alternative allele keep the alignment with the best score        
                {
                    if(pMate->b->core.qual<iAlignmentScore)
                    {
                        mAlignments[sQueryName]=pAlignments;
                    }
                }
            
                else if(bIsAlt)                                         //If the current alignment shows the alternative allele and the other not keep this alignment
                {
                    mAlignments[sQueryName]=pAlignments;
                }
            }
        }        
        
        //----------------------------------------------------------------
        //Alignment is HQ?
        //----------------------------------------------------------------
        
        if(bIsHQ)
        {
            //----------------------------------------------------------------
            //Compute HQ bias
            //----------------------------------------------------------------

            iHQReadDepth[iStrand]++;
            iHQAltReadDepth[iStrand]+=bIsAlt;
            
            //----------------------------------------------------------------
            //Accumulate HQ fragments
            //----------------------------------------------------------------
            
            itAlignments=mHQAlignments.find(sQueryName);
        
            if(itAlignments==mHQAlignments.end())                       //This name does not already exist?
            {
                mHQAlignments[sQueryName]=pAlignments;
            }
        
            else                                                        //This name does already exist
            {
                pMate=itAlignments->second;
            
                if(bIsSNV)
                {
                    if(bam1_qual(pMate->b)[pMate->qpos]<iBaseScore)     //For SNPs keep base with best score
                    {
                        mHQAlignments[sQueryName]=pAlignments;
                    }
                }
            
                else
                {
                    if((pMate->indel==iLen)==bIsAlt)                    //If both mates show the same alternative allele keep the alignment with the best score        
                    {
                        if(pMate->b->core.qual<iAlignmentScore)
                        {
                            mHQAlignments[sQueryName]=pAlignments;
                        }
                    }
            
                    else if(bIsAlt)                                     //If the current alignment shows the alternative allele and the other not keep this alignment
                    {
                        mHQAlignments[sQueryName]=pAlignments;
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------
    //Iterate over fragments
    //----------------------------------------------------------------
    
    iDepth=0;
    iAltDepth=0;
    
    itAlignmentsEnd=mAlignments.end();
    
    for(itAlignments=mAlignments.begin();itAlignments!=itAlignmentsEnd;itAlignments++)
    {
        pAlignments=itAlignments->second;

        if(bIsSNV)  
        {
            bIsAlt=int(bam1_seqi(bam1_seq(pAlignments->b),pAlignments->qpos))==iAlt;
        }
        
        else
        {
            bIsAlt=pAlignments->indel==iLen;
        }

        iDepth++;
        iAltDepth+=bIsAlt;
    }

    //----------------------------------------------------------------
    //Iterate over HQ fragments
    //----------------------------------------------------------------
    
    iHQDepth=0;
    iHQAltDepth=0;
    
    itAlignmentsEnd=mHQAlignments.end();
    
    for(itAlignments=mHQAlignments.begin();itAlignments!=itAlignmentsEnd;itAlignments++)
    {
        pAlignments=itAlignments->second;

        if(bIsSNV)  
        {
            bIsAlt=int(bam1_seqi(bam1_seq(pAlignments->b),pAlignments->qpos))==iAlt;
        }
        
        else
        {
            bIsAlt=pAlignments->indel==iLen;
        }

        iHQDepth++;
        iHQAltDepth+=bIsAlt;
    }
    
    //----------------------------------------------------------------
    //Save results
    //----------------------------------------------------------------
    
    iTotalReadDepth=max(iReadDepth[0]+iReadDepth[1],1);
    dInvTotalReadDepth=1.0/double(iTotalReadDepth);
    dInvTotalAltReadDepth=1.0/double(max(iAltReadDepth[0]+iAltReadDepth[1],1));
    
    pStatistics->iDepth=iDepth;
    pStatistics->fBias=float(double(iReadDepth[0])*dInvTotalReadDepth);
    pStatistics->iAltDepth=iAltDepth;
    pStatistics->fAltBias=float(double(iAltReadDepth[0])*dInvTotalAltReadDepth);
    
    pStatistics->iHQDepth=iHQDepth;
    pStatistics->fHQBias=float(double(iHQReadDepth[0])/double(max(iHQReadDepth[0]+iHQReadDepth[1],1)));
    pStatistics->iHQAltDepth=iHQAltDepth;
    pStatistics->fHQAltBias=float(double(iHQAltReadDepth[0])/double(max(iHQAltReadDepth[0]+iHQAltReadDepth[1],1)));
    
    pStatistics->fDuplicates=float(double(nDuplicates)/double(max(nAlignments,1)));
    pStatistics->fRefSkip=float(double(nRefSkip)/double(nRefSkip+iTotalReadDepth));
    pStatistics->fSecondary=float(double(nSecondary)*dInvTotalReadDepth);
    pStatistics->fSecondaryAlt=float(double(nSecondaryAlt)*dInvTotalAltReadDepth);
    pStatistics->fOrphan=float(double(nOrphan)*dInvTotalReadDepth);
    pStatistics->fOrphanAlt=float(double(nOrphanAlt)*dInvTotalAltReadDepth);
    pStatistics->fAnomalous=float(double(nAnomalous)*dInvTotalReadDepth);
    pStatistics->fAnomalousAlt=float(double(nAnomalousAlt)*dInvTotalAltReadDepth);
    pStatistics->fAlternatives=float(double(nAlternatives)*dInvTotalReadDepth);
    pStatistics->fAlternativesAlt=float(double(nAlternativesAlt)*dInvTotalAltReadDepth);
    pStatistics->fSuboptimal=float(double(nSuboptimal)*dInvTotalReadDepth);
    pStatistics->fSuboptimalAlt=float(double(nSuboptimalAlt)*dInvTotalAltReadDepth);
    pStatistics->fClipped=float(double(nClipped)*dInvTotalReadDepth);
    pStatistics->fClippedAlt=float(double(nClippedAlt)*dInvTotalAltReadDepth);
    pStatistics->fMatchLengthTooLow=float(double(nMatchLengthTooLow)*dInvTotalReadDepth);
    pStatistics->fMatchLengthTooLowAlt=float(double(nMatchLengthTooLowAlt)*dInvTotalAltReadDepth);
    pStatistics->fMismatchesTooHigh=float(double(nMismatchesTooHigh)*dInvTotalReadDepth);
    pStatistics->fMismatchesTooHighAlt=float(double(nMismatchesTooHighAlt)*dInvTotalAltReadDepth);
    
    pStatistics->fGenotypingError=float(double(nGenotypeErrors)*dInvTotalReadDepth);
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------
}
//----------------------------------------------------------------
void * CAlgorithm::Pileup(CAlgorithm * pAlgorithm)
{
    multimap<int,ENTRY> *   pAnnovarFileEntries;

    JOB                     job;
    COMPUTE_STATISTICS      computeStatistics;

    CBamFile                bamFile;
    
    string                  sRegion;    
    
    map<string,TARGET>::iterator                                        itFastaFileTargets;
    map<string,multimap<int,int> >::iterator                            itVCFFileEntries;
    pair<multimap<int,ENTRY>::iterator,multimap<int,ENTRY>::iterator>   equalRange;
    multimap<int,ENTRY>::iterator                                       itEqualRange;
    
    //----------------------------------------------------------------
    //Initialize compute statistics
    //----------------------------------------------------------------
    
    computeStatistics.bCountDuplicates=pAlgorithm->bCountDuplicates;
    computeStatistics.bSecondaryIsHQ=pAlgorithm->bSecondaryIsHQ;
    computeStatistics.bOrphanIsHQ=pAlgorithm->bOrphanIsHQ;
    computeStatistics.bAnomalousIsHQ=pAlgorithm->bAnomalousIsHQ;
    computeStatistics.bHasAlternativesIsHQ=pAlgorithm->bHasAlternativesIsHQ;
    computeStatistics.bSuboptimalIsHQ=pAlgorithm->bSuboptimalIsHQ;
    computeStatistics.bClippedIsHQ=pAlgorithm->bClippedIsHQ;
    computeStatistics.bMatchLengthTooLowIsHQ=pAlgorithm->bMatchLengthTooLowIsHQ;
    computeStatistics.bMismatchesTooHighIsHQ=pAlgorithm->bMismatchesTooHighIsHQ;

    computeStatistics.iMinBaseScore=pAlgorithm->iMinBaseScore;
    computeStatistics.iMinMismatchBaseScore=pAlgorithm->iMinMismatchBaseScore;
    computeStatistics.iMinAlignmentScore=pAlgorithm->iMinAlignmentScore;
    computeStatistics.nMaxMismatches=pAlgorithm->nMaxMismatches;
    computeStatistics.iMinDiffSuboptimal=pAlgorithm->iMinDiffSuboptimal;
    
    computeStatistics.dMinMatchLength=pAlgorithm->dMinMatchLength;

    //----------------------------------------------------------------
    //Run
    //----------------------------------------------------------------
    
    pAlgorithm->mutex.Lock();
    
    for(;;)
    {
        //----------------------------------------------------------------
        //Get job from queue
        //----------------------------------------------------------------
        
        if(pAlgorithm->jobs.size()==0)
        {
            pAlgorithm->mutex.Unlock();
            pthread_exit(NULL);
        }

        job=pAlgorithm->jobs.front();
        pAlgorithm->jobs.pop_front();

        pAlgorithm->mutex.Unlock();

        //----------------------------------------------------------------
        //Open bam file
        //----------------------------------------------------------------
        
        if(bamFile.Open(pAlgorithm->vBamFileNames[job.iBamFile],0,true)==false)
        {
            cerr << "Error: Could not open bam file: " << pAlgorithm->vBamFileNames[job.iBamFile] << " (skipping job)" << endl;
            pAlgorithm->mutex.Lock();
            pAlgorithm->nJobsDone++;
            continue;
        }
        
        //----------------------------------------------------------------
        //Set target on bam file
        //----------------------------------------------------------------

        if(pAlgorithm->bPileupRegions==false)
        {
            sRegion=job.sTarget;
        }
        
        else
        {
            sRegion=job.sTarget+string(":")+to_string(max(job.iPos-10,1))+string("-")+to_string(job.iPos+10);
        }

        if(bamFile.SetRegion(sRegion)==false)
        {
            cerr << "Error: Could not set region: " << sRegion << " (skipping job)" << endl;
            pAlgorithm->mutex.Lock();
            pAlgorithm->nJobsDone++;
            continue;
        }

        computeStatistics.pPileupData=bamFile.GetPileupData();
        
        //----------------------------------------------------------------
        //Get target region from fasta file
        //----------------------------------------------------------------
        
        if((itFastaFileTargets=pAlgorithm->fastaFile.targets.find(job.sTarget))!=pAlgorithm->fastaFile.targets.end())
        {
            computeStatistics.pTarget=itFastaFileTargets->second.pTarget;
        }
        
        else
        {
            cerr << "Error: Target not present in reference: " << job.sTarget << " (skipping job)" << endl;
            pAlgorithm->mutex.Lock();
            pAlgorithm->nJobsDone++;
            continue;            
        }

        //----------------------------------------------------------------
        //Get pointers to vcf file
        //----------------------------------------------------------------
        
        if((itVCFFileEntries=pAlgorithm->vcfFile.mEntries.find(job.sTarget))!=pAlgorithm->vcfFile.mEntries.end())
        {
            computeStatistics.pVCFFileEntries=&itVCFFileEntries->second;
        }
        
        else
        {
            cerr << "Warning: No SNP entries in vcf for target: " << job.sTarget <<  endl;
            computeStatistics.pVCFFileEntries=NULL;
        }
        
        //----------------------------------------------------------------
        //Get pointer to annovar file
        //----------------------------------------------------------------
        
        pAnnovarFileEntries=&pAlgorithm->annovarFile.mEntries.at(job.sTarget);
        
        //----------------------------------------------------------------
        //Pileup
        //----------------------------------------------------------------
        
        if(pAlgorithm->bPileupRegions==false)
        {
            while(bamFile.PileupRegion())
            {
                equalRange=pAnnovarFileEntries->equal_range(computeStatistics.pPileupData->iTargetPos);

                for(itEqualRange=equalRange.first;itEqualRange!=equalRange.second;itEqualRange++)
                {
                    computeStatistics.iAlt=itEqualRange->second.iAlt;
                    computeStatistics.iLen=itEqualRange->second.iLen;
                    computeStatistics.pStatistics=((STATISTICS*)itEqualRange->second.pUserData)+job.iBamFile;

                    ComputeStatistics(computeStatistics);
                }
            }
        }

        //----------------------------------------------------------------
        //Pileup regions
        //----------------------------------------------------------------
        
        else
        {
            while(bamFile.PileupRegion())
            {
                if(computeStatistics.pPileupData->iTargetPos==job.iPos) 
                {
                    equalRange=pAnnovarFileEntries->equal_range(computeStatistics.pPileupData->iTargetPos);

                    for(itEqualRange=equalRange.first;itEqualRange!=equalRange.second;itEqualRange++)
                    {
                        computeStatistics.iAlt=itEqualRange->second.iAlt;
                        computeStatistics.iLen=itEqualRange->second.iLen;
                        computeStatistics.pStatistics=((STATISTICS*)itEqualRange->second.pUserData)+job.iBamFile;

                        ComputeStatistics(computeStatistics);
                    }
                }
            }
        }
        
        //----------------------------------------------------------------
        //Job done
        //----------------------------------------------------------------
        
        pAlgorithm->mutex.Lock();
        pAlgorithm->nJobsDone++;
    }
}
//----------------------------------------------------------------
void CAlgorithm::CreateOutputMatrix(void)
{
    int nEntries;
    int nBamFiles;

    STATISTICS * pStatistics;    
    
    map<string,multimap<int,ENTRY> >::iterator  itTarget;
    map<string,multimap<int,ENTRY> >::iterator  itTargetEnd;
    multimap<int,ENTRY>::iterator   itPos;
    multimap<int,ENTRY>::iterator   itPosEnd;
    
    nEntries=annovarFile.nEntries;
    nBamFiles=vBamFileNames.size();
    
    pStatistics=(STATISTICS*)memset(new STATISTICS[nEntries*nBamFiles],0,nEntries*nBamFiles*sizeof(STATISTICS));

    itTargetEnd=annovarFile.mEntries.end();
    
    for(itTarget=annovarFile.mEntries.begin();itTarget!=itTargetEnd;itTarget++)
    {
        itPosEnd=itTarget->second.end();
        
        for(itPos=itTarget->second.begin();itPos!=itPosEnd;itPos++,pStatistics+=nBamFiles)
        {
            itPos->second.pUserData=pStatistics;
        }
    }
}
//----------------------------------------------------------------
void CAlgorithm::CreateJobs(void)
{
    int nBamFiles;
    JOB job;
    
    map<string,multimap<int,ENTRY> >::iterator  itTarget;
    map<string,multimap<int,ENTRY> >::iterator  itTargetEnd;
    multimap<int,ENTRY>::iterator   itPos;
    multimap<int,ENTRY>::iterator   itPosEnd;

    nBamFiles=vBamFileNames.size();
    
    itTargetEnd=annovarFile.mEntries.end();
    
    if(bPileupRegions)  
    {
        for(job.iBamFile=0;job.iBamFile<nBamFiles;job.iBamFile++)
        {
            for(itTarget=annovarFile.mEntries.begin();itTarget!=itTargetEnd;itTarget++)
            {
                job.iPos=-1;
                job.sTarget=itTarget->first;
                
                itPosEnd=itTarget->second.end();

                for(itPos=itTarget->second.begin();itPos!=itPosEnd;itPos++)
                {
                    if(job.iPos!=itPos->first)  
                    {
                        job.iPos=itPos->first;
                        jobs.push_back(job);
                    }
                }
            }
        }
    }
    
    else                        
    {
        job.iPos=-1;
        
        for(job.iBamFile=0;job.iBamFile<nBamFiles;job.iBamFile++)
        {
            for(itTarget=annovarFile.mEntries.begin();itTarget!=itTargetEnd;itTarget++)
            {
                job.sTarget=itTarget->first;
                jobs.push_back(job);
            }
        }
    }
}
//----------------------------------------------------------------
void CAlgorithm::RunJobs(void)
{
    int         nJobs;
    int         iThread;
    
    pthread_t * pThreads;
    void *      pThreadReturn;
    
    nJobs=jobs.size();
    nJobsDone=0;

    //----------------------------------------------------------------
    //Create worker threads
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Create worker threads" << endl;
    
    pThreads=new pthread_t[nThreads];
    
    for(iThread=0;iThread<nThreads;iThread++)
    {
        pthread_create(&pThreads[iThread],NULL,(void*(*)(void*))Pileup,(void*)this);
    }

    //----------------------------------------------------------------
    //Wait for threads to finish
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Wait for worker threads to finish" << endl;
    
    for(iThread=0;iThread<nThreads;iThread++)
    {
        while(pthread_tryjoin_np(pThreads[iThread],(void**)&pThreadReturn))
        {
            if(bVerbose) cerr << cProgressBar[min((nJobsDone*100)/nJobs,100)] << '\r' << flush;
            sleep(1);
        }
    }
    
    if(nJobsDone!=nJobs)
    {
        cerr << "Error: Number of jobs done differs from number of jobs" << endl;
    }
    
    if(bVerbose) cerr << cProgressBar[100]  << endl;
    

    delete [] pThreads; 
}
//----------------------------------------------------------------
void CAlgorithm::WriteOutputMatrix(void)
{
    int nBamFiles;
    int iBamFile;
    
    double  dInvDepth;
    double  dInvHQDepth;
    
    STATISTICS * pStatistics;
    
    map<string,multimap<int,ENTRY> >::iterator  itTarget;
    map<string,multimap<int,ENTRY> >::iterator  itTargetEnd;
    multimap<int,ENTRY>::iterator   itPos;
    multimap<int,ENTRY>::iterator   itPosEnd;
    
    //----------------------------------------------------------------
    //Header
    //----------------------------------------------------------------
    
    cout << annovarFile.sHeader;
    
    nBamFiles=vBamFileNames.size();
    
    for(iBamFile=0;iBamFile<nBamFiles;iBamFile++)
    {
        cout    << "\t___"
                "\t(" << SampleNameFromPath(vBamFileNames[iBamFile]) << ")depth"    //Fragments
                "\tbias"                                                            //Reads (all)
                "\talt depth"                                                       //Fragments
                "\talt freq"                                                        //Fragments
                "\talt bias"                                                        //Reads

                "\tHQ depth"                                                        //Fragments
                "\tHQ bias"                                                         //Reads
                "\tHQ alt depth"                                                    //Fragments
                "\tHQ alt freq"                                                     //Fragments
                "\tHQ alt bias"                                                     //Reads

                "\tHQ ratio"                                                        //Fragments
                "\tHQ alt ratio"                                                    
               
                "\tduplicates"                                                      //Fragments
                "\trefskip"                                                         //Reads
                "\tsecondary"                                                       //Fragments
                "\tsecondaryAlt"                                                    //Fragments
                "\torphan"                                                          //Fragments
                "\torphanAlt"                                                       //Fragments
                "\tanomalous"                                                       //Fragments
                "\tanomalousAlt"                                                    //Fragments
                "\talternatives"                                                    //Fragments
                "\talternativesAlt"                                                 //Fragments
                "\tsuboptimal"                                                      //Fragments
                "\tsuboptimalAlt"                                                   //Fragments
                "\tclipped"                                                         //Reads
                "\tclippedAlt"                                                      //Reads
                "\tmatchLengthTooLow"                                               //Reads
                "\tmatchLengthTooLowAlt"                                            //Reads
                "\tmismatchesTooHigh"                                               //Reads
                "\tmismatchesTooHighAlt"                                            //Reads
               
                "\tgenotypingError";                                                //Reads
    }
    
    cout << '\n';

    //----------------------------------------------------------------
    //Entries
    //----------------------------------------------------------------
    
    itTargetEnd=annovarFile.mEntries.end();
    
    for(itTarget=annovarFile.mEntries.begin();itTarget!=itTargetEnd;itTarget++)
    {
        itPosEnd=itTarget->second.end();
        
        for(itPos=itTarget->second.begin();itPos!=itPosEnd;itPos++)
        {
            cout << itPos->second.sLine;

            pStatistics=(STATISTICS*)itPos->second.pUserData;

            for(iBamFile=0;iBamFile<nBamFiles;iBamFile++,pStatistics++)
            {
                dInvDepth=1.0/double(max(pStatistics->iDepth,1));
                dInvHQDepth=1.0/double(max(pStatistics->iHQDepth,1));

                cout    << '\t'
                        << '\t' << pStatistics->iDepth
                        << '\t' << pStatistics->fBias
                        << '\t' << pStatistics->iAltDepth
                        << '\t' << float(double(pStatistics->iAltDepth)*dInvDepth)
                        << '\t' << pStatistics->fAltBias

                        << '\t' << pStatistics->iHQDepth
                        << '\t' << pStatistics->fHQBias
                        << '\t' << pStatistics->iHQAltDepth
                        << '\t' << float(double(pStatistics->iHQAltDepth)*dInvHQDepth)
                        << '\t' << pStatistics->fHQAltBias

                        << '\t' << float(double(pStatistics->iHQDepth)*dInvDepth)
                       << '\t' << float(double(pStatistics->iHQAltDepth)/double(max(pStatistics->iAltDepth,1)))
                        
                        << '\t' << pStatistics->fDuplicates
                        << '\t' << pStatistics->fRefSkip
                        << '\t' << pStatistics->fSecondary
                        << '\t' << pStatistics->fSecondaryAlt
                        << '\t' << pStatistics->fOrphan
                        << '\t' << pStatistics->fOrphanAlt
                        << '\t' << pStatistics->fAnomalous
                        << '\t' << pStatistics->fAnomalousAlt
                        << '\t' << pStatistics->fAlternatives
                        << '\t' << pStatistics->fAlternativesAlt
                        << '\t' << pStatistics->fSuboptimal
                        << '\t' << pStatistics->fSuboptimalAlt
                        << '\t' << pStatistics->fClipped
                        << '\t' << pStatistics->fClippedAlt
                        << '\t' << pStatistics->fMatchLengthTooLow
                        << '\t' << pStatistics->fMatchLengthTooLowAlt
                        << '\t' << pStatistics->fMismatchesTooHigh
                        << '\t' << pStatistics->fMismatchesTooHighAlt
                        
                        << '\t' << pStatistics->fGenotypingError;
            }
            
            cout << '\n';
        }
    }
}
//----------------------------------------------------------------
CAlgorithm::CAlgorithm(void)
{
    bCountDuplicates=false;
    bPileupRegions=false;
    bVerbose=false;
    bSecondaryIsHQ=false;
    bOrphanIsHQ=false;
    bAnomalousIsHQ=false;
    bHasAlternativesIsHQ=false;
    bSuboptimalIsHQ=false;
    bClippedIsHQ=false;
    bMatchLengthTooLowIsHQ=false;
    bMismatchesTooHighIsHQ=false;
    
    nThreads=1;
    iMinBaseScore=30;
    iMinMismatchBaseScore=10;
    iMinAlignmentScore=40;
    nMaxMismatches=4;
    iMinDiffSuboptimal=10;
    
    dMinMatchLength=0.6;    
}
//----------------------------------------------------------------
bool CAlgorithm::Run(void)
{
    //----------------------------------------------------------------
    //Open Annovar file
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Open annovar file containing the variants" << endl;
    
    if(annovarFile.Open(sAnnovarFileName)==false)
    {
        return false;
    }
    
    //----------------------------------------------------------------
    //Open fasta file
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Open fasta file containing the reference sequences" << endl;
    
    if(fastaFile.Open(sFastaFileName)==false)
    {
        return false;
    }
    
    //----------------------------------------------------------------
    //Open vcf file
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Open vcf file containing the SNPs" << endl;
    
    if(vcfFile.Open(sVCFFileName)==false)
    {
        return false;
    }
    
    //----------------------------------------------------------------
    //Create output matrix
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Create output matrix" << endl;
    
    CreateOutputMatrix();
    
    //----------------------------------------------------------------
    //Create jobs
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Create jobs" << endl;
    
    CreateJobs();

    //----------------------------------------------------------------
    //Create jobs
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Run jobs" << endl;
    
    RunJobs();
    
    //----------------------------------------------------------------
    //Output statistics
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Write output matrix" << endl;
    
    WriteOutputMatrix();
    
    //----------------------------------------------------------------
    //Clean up memory
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Clean up" << endl;
    
    delete [] (STATISTICS*)annovarFile.mEntries.begin()->second.begin()->second.pUserData;
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------
    
    if(bVerbose) cerr << "Info: Done" << endl;
    
    
    return true;
}
//----------------------------------------------------------------