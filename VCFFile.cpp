//----------------------------------------------------------------
// Name        : VCFFile.cpp
// Author      : Remco Hoogenboezem
// Version     :
// Copyright   :
// Description :
//----------------------------------------------------------------
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "Alphabet.h"
#include "VCFFile.h"
//----------------------------------------------------------------
bool CVCFFile::Open(const string & sVCFFileName)
{
    bool    bGood;
    
    char    cLine[1048576];

    int     iAlt;
    
    FILE *  pVCFFile;
    
    char *  pLine;
    char *  pChrom;
    char *  pPos;
    char *  pRef;
    char *  pAlt;
    char *  pSubAlt;
    
    //----------------------------------------------------------------
    //Clear previous hash map if any (mEntries is probably very big)
    //----------------------------------------------------------------
    
    mEntries.clear();
    
    //----------------------------------------------------------------
    //Open vcf file
    //----------------------------------------------------------------
    
    if((pVCFFile=popen((string("bzcat -f ")+sVCFFileName+string(" | zcat -f")).c_str(),"r"))==NULL)
    {
        cerr << "Error: Could not open vcf file: " << sVCFFileName << endl;
        return false;
    }
   
    //----------------------------------------------------------------
    //Read header lines
    //----------------------------------------------------------------

    if(fgets(cLine,1048576,pVCFFile)==NULL)
    {
        cerr << "Error: Could not read header from vcf file: " << sVCFFileName << endl;
        pclose(pVCFFile);
        return false;
    }
    
    if(strstr(cLine,"##fileformat=VCFv4")==NULL)
    {
        cerr << "Error: Unexpected file format vcf file: " << sVCFFileName << endl;
        pclose(pVCFFile);
        return false;
    }
    
    for(bGood=fgets(cLine,1048576,pVCFFile)!=NULL;bGood && cLine[0]=='#';bGood=fgets(cLine,1048576,pVCFFile)!=NULL);
    
    //----------------------------------------------------------------
    //Read through the file
    //----------------------------------------------------------------
    
    for(;bGood;bGood=fgets(cLine,1048576,pVCFFile)!=NULL)
    {
        //----------------------------------------------------------------
        //Skip empty lines if any
        //----------------------------------------------------------------
        
        if(cLine[0]=='\n' || cLine[0]=='\0')
        {
            continue;
        }
        
        //----------------------------------------------------------------
        //Tokenize
        //----------------------------------------------------------------
        
        pLine=cLine;
        pChrom=strsep(&pLine,"\t");
        pPos=strsep(&pLine,"\t");
        strsep(&pLine,"\t");
        pRef=strsep(&pLine,"\t");
        pAlt=strsep(&pLine,"\t");
        
        if(pAlt==NULL)
        {
            cerr << "Error: Insufficient number of fields in vcf file: " << sVCFFileName << endl;
            pclose(pVCFFile);
            return false;
        }
        
        //----------------------------------------------------------------
        //Entry is point mutation or deletion
        //----------------------------------------------------------------
        
        if((strlen(pRef)==1)&&(iAscii2BamSeq[pRef[0]]!=N))
        {
            while((pSubAlt=strsep(&pAlt,","))!=NULL)
            {
                if((strlen(pSubAlt)!=1)||((iAlt=iAscii2BamSeq[pSubAlt[0]])==N))
                {
                    continue;
                }
            
                mEntries[pChrom].insert(pair<int,int>(atoi(pPos),iAlt));
            }
        }
    }

    pclose(pVCFFile);
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------

    return true;
}
//----------------------------------------------------------------
