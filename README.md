# AnnotateBAMStatisticsPro

This repository contains the code of AnnotateBAMStatisticsPro. AnnotateBAMStatistics is a multi-threaded algorithm for annotating ANNOVAR output with additional useful fragment count statistics for one ore more specified BAM files. This includes fragment coverage, number of mutant fragments, fragment-based variant allele frequency and mutant read bias for high quality and all fragments separately. The code for the base version of AnnotateBAMStatistics is available here:

AnnotateBAMStatisticsPro is a more refined version that includes additional statistics such as number of clipped reads, reads containing multiple non-SNP mismatches or suboptimal alignments. Please note that some of these statistics rely on BWA alignment, which is recommended. 

## Installation instructions

First clone the GitHub repository with git:

```
git clone https://gitlab.com/erasmusmc-hematology/annotatebamstatistics
```

Then you need to compile the copy of samtools that comes packaged in this repository. This is necessary because AnnotateBAMStatistics relies on a specific version of samtools and may not work with newer versions.

```
cd annotatebamstatistics/samtools
make
```

Finally, go back to the root directory of AnnotateBAMStatistics and run make again:

```
make all
```

The binary AnnotateBAMStatistics is located at:

```bash
./dist/Debug/GNU-Linux/annotate_bam_statistics_pro
``` 

## How do I run it?

AnnotateBAMStatisticsPro takes an ANNOVAR file and one or more BAM files (comma-separated list) as input and adds informative fragment-based statistics. These statistics are generally used for filtering the variant list or to determine clonality in cancer samples. AnnotateBAMStatisticsPro writes the output to standard output.

The following parameters are available:

- -b/--bam-files <text>:    One or more bam files (required)
- -a/--annovar-file <text>:     Single annovar file from table_annovar.pl script (required)
- -f/--fasta-file <text>:       Single fasta file with reference sequence (required)
- -v/--vcf-file <text>:     Single vcf file with SNPs (required)
- -t/--threads <int>:       Threads (optional default=1)
- -s/--min-base-score <int>:        Min base score (optional default=30)
- -k/--min-mismatch-base-score <int>:       Min mismatch base score (optional default=10)
- -S/--min-alignment-score <int>:       Min alignment score (optional,default=40)
- -m/--min-match-length <float>:        Min match length (optional default=0.6)
- -M/--max-mismatches <int>:        Max mismatches (optional default=4)
- -d/--min-diff-suboptimal <int>:       Min difference in alignment score between optimal and suboptimal (optional default=10)
- -c/--count-duplicates <void>:     If specified duplicates are used in the statistics (optional default=false)
- -r/--pileup-regions <void>:       If specified individual regions from the annovar files are piled up (optional default=false)
- -1/--secondary-is-HQ <void>:      Count secondary alignments as HQ (optional default=false)
- -2/--orphan-is-HQ <void>:     Count orphan alignments as HQ (optional default=false)
- -3/--anomalous-is-HQ <void>:      Count anomalous alignments as HQ (optional default=false)
- -4/--has-alternatives-is-HQ <void>:       Count alignments with alternatives as HQ (optional default=false)
- -5/--suboptimal-is-HQ <void>:     Count suboptimal alignments as HQ (optional default=false)
- -6/--clipped-is-HQ <void>:        Count clipped alignments as HQ (optional default=false)
- -7/--match-length-too-low-is-HQ <void>:       Count alignments with a low match length as HQ (optional default=false)
- -8/--mismatches-too-high-is-HQ <void>:        Count alignments with too many mismatches as HQ (optional default=false)
- -V/--verbose <void>:      Be verbose and show progress (optional default=false)

*Dependencies*
- gcc 4.3+
- g++ 4.3+

## Acknowledgements & Citation

This program was written by Remco Hoogenboezem at Erasmus MC. Please cite this repository if you use this code in your research.
