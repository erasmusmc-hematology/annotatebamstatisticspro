//----------------------------------------------------------------
// Name        : main.cpp
// Author      : Remco Hoogenboezem
// Version     :
// Copyright   :
// Description : annotate bam statistics
//----------------------------------------------------------------
#include <iostream>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include "Algorithm.h"
//----------------------------------------------------------------
#define BAM_FILES                   'b'
#define ANNOVAR_FILE                'a'
#define FASTA_FILE                  'f'
#define VCF_FILE                    'v'
#define THREADS                     't'
#define MIN_BASE_SCORE              's'
#define MIN_MISMATCH_BASE_SCORE     'k'
#define MIN_ALIGNMENT_SCORE         'S'
#define MIN_MATCH_LENGTH            'm'
#define MAX_MISMATCHES              'M'
#define MIN_DIFF_SUBOPTIMAL         'd'
#define COUNT_DUPLICATES            'c'
#define PILEUP_REGIONS              'r'
#define SECONDARY_IS_HQ             '1'
#define ORPHAN_IS_HQ                '2'
#define ANOMALOUS_IS_HQ             '3'
#define HAS_ALTERNATIVES_IS_HQ      '4'
#define SUBOPTIMAL_IS_HQ            '5'
#define CLIPPED_IS_HQ               '6'
#define MATCH_LENGTH_TOO_LOW_IS_HQ  '7'
#define MISMATCHES_TOO_HIGH_IS_HQ   '8'
#define VERBOSE                     'V'
#define HELP                        'h'
#define SHORT_OPTIONS               "b:n:a:f:v:t:s:k:S:m:M:d:cr12345678Vh"
//----------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------
struct option longOptions[] =
{
    {"bam-files",required_argument,NULL,BAM_FILES},
    {"annovar-file",required_argument,NULL,ANNOVAR_FILE},
    {"fasta-file",required_argument,NULL,FASTA_FILE},
    {"vcf-file",required_argument,NULL,VCF_FILE},
    {"threads",required_argument,NULL,THREADS},
    {"min-base-score",required_argument,NULL,MIN_BASE_SCORE},
    {"min-mismatch-base-score",required_argument,NULL,MIN_MISMATCH_BASE_SCORE},
    {"min-alignment-score",required_argument,NULL,MIN_ALIGNMENT_SCORE},
    {"min-match-length",required_argument,NULL,MIN_MATCH_LENGTH},
    {"max-mismatches",required_argument,NULL,MAX_MISMATCHES},
    {"min-diff-suboptimal",required_argument,NULL,MIN_DIFF_SUBOPTIMAL},
    {"count-duplicates",no_argument,NULL,COUNT_DUPLICATES},
    {"pileup-regions",no_argument,NULL,PILEUP_REGIONS},
    {"secondary-is-HQ",no_argument,NULL,SECONDARY_IS_HQ},
    {"orphan-is-HQ",no_argument,NULL,ORPHAN_IS_HQ},
    {"anomalous-is-HQ",no_argument,NULL,ANOMALOUS_IS_HQ},
    {"has-alternatives-is-HQ",no_argument,NULL,HAS_ALTERNATIVES_IS_HQ},
    {"suboptimal-is-HQ",no_argument,NULL,SUBOPTIMAL_IS_HQ},
    {"clipped-is-HQ",no_argument,NULL,CLIPPED_IS_HQ},
    {"match-length-too-low-is-HQ",no_argument,NULL,MATCH_LENGTH_TOO_LOW_IS_HQ},
    {"mismatches-too-high-is-HQ",no_argument,NULL,MISMATCHES_TOO_HIGH_IS_HQ},
    {"verbose",no_argument,NULL,VERBOSE},
    {"help",no_argument,NULL,HELP},
    {0, 0, 0, 0}
};
//----------------------------------------------------------------
vector<string> & Tokenize(const string & sString,const char * pDelimiter)
{
    int                     iStrLen;
    char *                  pString;
    char *                  pStringTemp;
    static vector<string>   vTokens;
    
    vTokens.clear();
    
    if((iStrLen=sString.size())>0)
    {
        pString=pStringTemp=(char*)memcpy(new char[iStrLen+1],sString.c_str(),iStrLen+1);

        while(pStringTemp)
        {
            vTokens.push_back(string(strsep(&pStringTemp,pDelimiter)));
        }
    
        delete [] pString;
    }
    
    return vTokens;
}
//----------------------------------------------------------------
int main(int argc, char** argv)
{
    bool        bShowHelp;
    
    int         iOption;
    int         iOptionIndex;
    
    CAlgorithm  algorithm;
    
    //----------------------------------------------------------------
    //Get options
    //----------------------------------------------------------------
    
    bShowHelp=(argc==1);
    
    while((iOption=getopt_long(argc,argv,SHORT_OPTIONS,longOptions,&iOptionIndex))>=0)
    {
        switch(iOption)
        {
            case BAM_FILES:
                
                algorithm.vBamFileNames=Tokenize(string(optarg),",");
                break;

            case ANNOVAR_FILE:
                
                algorithm.sAnnovarFileName=string(optarg);
                break;
               
            case FASTA_FILE:
                
                algorithm.sFastaFileName=string(optarg);
                break;
                
            case VCF_FILE:
                
                algorithm.sVCFFileName=string(optarg);
                break;
                
            case THREADS:
                
                algorithm.nThreads=atoi(optarg);
                break;
                
            case MIN_BASE_SCORE:
                
                algorithm.iMinBaseScore=atoi(optarg);
                break;
                
            case MIN_MISMATCH_BASE_SCORE:
                
                algorithm.iMinMismatchBaseScore=atoi(optarg);
                break;

            case MIN_ALIGNMENT_SCORE:
                
                algorithm.iMinAlignmentScore=atoi(optarg);
                break;
                
            case MIN_MATCH_LENGTH:
                
                algorithm.dMinMatchLength=atof(optarg);
                break;
                
            case MAX_MISMATCHES:
                
                algorithm.nMaxMismatches=atoi(optarg);
                break;
                
            case MIN_DIFF_SUBOPTIMAL:
                
                algorithm.iMinDiffSuboptimal=atoi(optarg);
                break;

            case COUNT_DUPLICATES:
                
                algorithm.bCountDuplicates=true;
                break;
                
            case PILEUP_REGIONS:
                
                algorithm.bPileupRegions=true;
                break;
                
            case SECONDARY_IS_HQ:
                
                algorithm.bSecondaryIsHQ=true;
                break;

            case ORPHAN_IS_HQ:
                
                algorithm.bOrphanIsHQ=true;
                break;
                
            case ANOMALOUS_IS_HQ:
                
                algorithm.bAnomalousIsHQ=true;
                break;
                
            case HAS_ALTERNATIVES_IS_HQ:
                
                algorithm.bHasAlternativesIsHQ=true;
                break;
                
            case SUBOPTIMAL_IS_HQ:
                
                algorithm.bSuboptimalIsHQ=true;
                break;
                
            case CLIPPED_IS_HQ:
                
                algorithm.bClippedIsHQ=true;
                break;
                
            case MATCH_LENGTH_TOO_LOW_IS_HQ:
                
                algorithm.bMatchLengthTooLowIsHQ=true;
                break;

            case MISMATCHES_TOO_HIGH_IS_HQ:
                
                algorithm.bMismatchesTooHighIsHQ=true;
                break;
                
            case VERBOSE:
                
                algorithm.bVerbose=true;
                break;
                
            case HELP:
            default:
                
                bShowHelp=true;
                break;
        }
    }
    
    //----------------------------------------------------------------
    //Show help
    //----------------------------------------------------------------

    if(bShowHelp)
    {
        cerr << "annotate_bam_statistics_pro [options] > annovarFileOut.txt (output always to std::out)"                                              << endl;
        cerr                                                                                                                                            << endl;
        cerr << "-b --bam-files <text>                  One or more bam files (required)"                                                               << endl;
        cerr << "-a --annovar-file <text>               Single annovar file from table_annovar.pl script (required)"                                    << endl;
        cerr << "-f --fasta-file <text>                 Single fasta file with reference sequence (required)"                                           << endl;
        cerr << "-v --vcf-file <text>                   Single vcf file with SNPs (required)"                                                           << endl;
        cerr << "-t --threads <int>                     Threads (optional default=1)"                                                                   << endl;
        cerr << "-s --min-base-score <int>              Min base score (optional default=30)"                                                           << endl;
        cerr << "-k --min-mismatch-base-score <int>     Min mismatch base score (optional default=10)"                                                  << endl;
        cerr << "-S --min-alignment-score <int>         Min alignment score (optional,default=40)"                                                      << endl;
        cerr << "-m --min-match-length <float>          Min match length (optional default=0.6)"                                                        << endl;
        cerr << "-M --max-mismatches <int>              Max mismatches (optional default=4)"                                                            << endl;
        cerr << "-d --min-diff-suboptimal <int>         Min difference in alignment score between optimal and suboptimal (optional default=10)"         << endl;
        cerr << "-c --count-duplicates <void>           If specified duplicates are used in the statistics (optional default=false)"                    << endl;
        cerr << "-r --pileup-regions <void>             If specified individual regions from the annovar files are piled up (optional default=false)"   << endl;
        cerr << "-1 --secondary-is-HQ <void>            Count secondary alignments as HQ (optional default=false)"                                      << endl;
        cerr << "-2 --orphan-is-HQ <void>               Count orphan alignments as HQ (optional default=false)"                                         << endl;
        cerr << "-3 --anomalous-is-HQ <void>            Count anomalous alignments as HQ (optional default=false)"                                      << endl;
        cerr << "-4 --has-alternatives-is-HQ <void>     Count alignments with alternatives as HQ (optional default=false)"                              << endl;
        cerr << "-5 --suboptimal-is-HQ <void>           Count suboptimal alignments as HQ (optional default=false)"                                     << endl;
        cerr << "-6 --clipped-is-HQ <void>              Count clipped alignments as HQ (optional default=false)"                                        << endl;
        cerr << "-7 --match-length-too-low-is-HQ <void> Count alignments with a low match length as HQ (optional default=false)"                        << endl;
        cerr << "-8 --mismatches-too-high-is-HQ <void>  Count alignments with too many mismatches as HQ (optional default=false)"                       << endl;
        cerr << "-V --verbose <void>                    Be verbose and show progress (optional default=false)"                                          << endl;
        cerr                                                                                                                                            << endl;
        
        return 0;
    }
    
    //----------------------------------------------------------------
    //Check options
    //----------------------------------------------------------------
    
    
    if(algorithm.vBamFileNames.size()==0)
    {
        cerr << "Error: Please specify at least one bam file! (-h for help)" << endl;
        return 1;
    }

    if(algorithm.sAnnovarFileName.empty())
    {
        cerr << "Error: Please specify an annovar file! (-h for help)" << endl;
        return 1;
    }
    
    if(algorithm.sFastaFileName.empty())
    {
        cerr << "Error: Please specify a fasta file! (-h for help)" << endl;
        return 1;
    }
    
    if(algorithm.sVCFFileName.empty())
    {
        cerr << "Error: Please specify a vcf file! (-h for help)" << endl;
        return 1;
    }
    
    //----------------------------------------------------------------
    //Lets go
    //----------------------------------------------------------------
    
    if(algorithm.Run()==false)
    {
        return 1;
    }
    
    //----------------------------------------------------------------
    //Done
    //----------------------------------------------------------------
    
    return 0;
}
//----------------------------------------------------------------
